import { question_1, question_2, question_3, question_4, question_7, question_8 } from "./questions";

console.log('\n');
console.log('Question 1:\n');
console.log(question_1.isNullOrEmpty(null));
console.log(question_1.isNullOrEmpty('a'));
console.log(question_1.isNullOrEmpty(''));
console.log(question_1.isNullOrEmpty('null'));

console.log('\n');
console.log('Question 2:\n');
console.log(question_2.findPositiveDivisors(12));
console.log(question_2.findPositiveDivisors(60));
console.log(question_2.findPositiveDivisors(42));

console.log('\n');
console.log('Question 3:\n');
console.log(question_3.getTriangleArea(3, 4, 5));

console.log('\n');
console.log('Question 4:\n');
console.log(question_4.findMostOccurrence([5, 4, 3, 2, 4, 5, 1, 6, 1, 2, 5, 4]));
console.log(question_4.findMostOccurrence([1, 2, 3, 4, 5, 1, 6, 7]));
console.log(question_4.findMostOccurrence([1, 2, 3, 4, 5, 6, 7]));

console.log('\n');
console.log('Question 7:\n');
const arrangeByName = question_7.arrangeBy('name');
console.log(arrangeByName(question_7.users));
const arrangeByName1 = question_7.arrangeBy('age');
console.log(arrangeByName1(question_7.users));
const arrangeByName2 = question_7.arrangeBy('id');
console.log(arrangeByName2(question_7.users));

console.log('\n');
console.log('Question 8:\n');
const htmlInput = '<div> <h1>Heading 1</h1> <a href="https://www.google.com">Read More</a> </div> <div> <h1>Heading 2</h1> <a href="https://www.google-fake.com">Read More</a> </div> <div> <a href="https://www.facebook.com"> <h1>Heading 3</h1> <a href="https://www.facebook-fake.com">Read More</a> </div>';
question_8.linkChecker(htmlInput);

