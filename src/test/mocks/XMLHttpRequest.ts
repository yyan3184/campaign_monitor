export interface MockXMLHttpRequestObject {
    open: () => void,
    onload: () => void;
    onerror: () => void;
    send: () => void;
    status: number;
}

export const createMockXMLHttpRequestObject = () => {
    return {
        open: jest.fn(),
        send: jest.fn(),
        onload: jest.fn(),
        onerror: jest.fn(),
        status: 200
    }
};

export const createMockXMLHttpRequestConstructor = (
    mockXMLHttpRequestObject: MockXMLHttpRequestObject
) => () => mockXMLHttpRequestObject;
