import * as utils from '../question_8';
import {
    createMockXMLHttpRequestConstructor,
    createMockXMLHttpRequestObject,
    MockXMLHttpRequestObject
} from './mocks/XMLHttpRequest'

const originalXMLHttpRequest = global.XMLHttpRequest;

describe("question_8", () => {
    describe("validateLink", () => {
        let mockXMLHttpRequestObject: MockXMLHttpRequestObject;
        beforeEach(() => {
            mockXMLHttpRequestObject = createMockXMLHttpRequestObject();
            (global.XMLHttpRequest as unknown) = jest.fn(
                createMockXMLHttpRequestConstructor(mockXMLHttpRequestObject)
            );
        });
        afterEach(() => {
            global.XMLHttpRequest = originalXMLHttpRequest;
        })

        it("should send a http request", (done) => {
            const link = 'https://www.google.com.au';
            utils.validateLink(link);
            expect(mockXMLHttpRequestObject.open).toHaveBeenCalledWith('GET', link);
            expect(mockXMLHttpRequestObject.send).toHaveBeenCalled();
            done();
        });


        it("should receive the link when the given link valid with 200 status", (done) => {
            const link = 'https://www.google.com.au';
            (mockXMLHttpRequestObject.onload as jest.Mock).mockImplementationOnce(
                () => Promise.resolve(link)
            );
            const validLinkPromise = utils.validateLink(link);

            mockXMLHttpRequestObject.onload();

            validLinkPromise.then(response => {
                expect(response).toEqual(link);
            });
            done();
        });

        it("should receive an empty string when the given link is invalid without 200 status", (done) => {
            const link = 'https://www.google123.com.au';
            const emptyString = '';
            (mockXMLHttpRequestObject.onerror as jest.Mock).mockImplementationOnce(
                () => Promise.resolve(emptyString)
            );
            const validLinkPromise = utils.validateLink(link);

            mockXMLHttpRequestObject.onerror();

            validLinkPromise.then(response => {
                expect(response).toEqual(emptyString);
            });
            done();
        });
    });


    describe("linkChecker", () => {
        let mockXMLHttpRequestObject: MockXMLHttpRequestObject;
        beforeEach(() => {
            mockXMLHttpRequestObject = createMockXMLHttpRequestObject();
            (global.XMLHttpRequest as unknown) = jest.fn(
                createMockXMLHttpRequestConstructor(mockXMLHttpRequestObject)
            );
        });
        afterEach(() => {
            global.XMLHttpRequest = originalXMLHttpRequest;
        })
        const htmltInput = '<html><head></head><body><div><a href="https://google.com">google</a></div><div><a href="https://google1.com">google1</a></div><div><a href="https://google2.com">google2</a></div><div><a href="https://google3.com">google3</a></div><div><a href="https://facebook.com">facebook</a></div><div><a href="https://facebook1.com">facebook1</a></div><div><a href="https://facebook2.com">facebook2</a></div></body></html>'

        it('should return only the expected valid links', (done) => {
            const expectedLinks = ['https://google.com', 'https://facebook.com'];

            jest.spyOn(utils, 'validateLink')
                .mockResolvedValueOnce('https://google.com')
                .mockResolvedValueOnce('')
                .mockResolvedValueOnce('')
                .mockResolvedValueOnce('')
                .mockResolvedValueOnce('https://facebook.com')
                .mockResolvedValueOnce('')
                .mockResolvedValueOnce('');
            const linkCheckerPromise = utils.linkChecker(htmltInput);

            linkCheckerPromise.then((links) => {
                expect(links).toEqual(expectedLinks);
            });
            done();
        });
    });
});
