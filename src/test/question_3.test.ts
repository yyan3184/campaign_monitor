import { getTriangleArea } from "../question_3";

describe("question_3/getTriangleArea", () => {
    const expectedError = 'InvalidTriangleException';
    it("should return 6 as the expected total area for triangle", () => {
        const expectedTotalArea = 6;
        const result = getTriangleArea(3, 4, 5);
        expect(result).toEqual(expectedTotalArea);
    });

    it("should throw an error if any of the given input is negative", () => {
        try {
            getTriangleArea(3, 4, -1);
        } catch (e) {
            expect((<Error>e).message).toEqual(expectedError);
        }
    });

    it("should throw an error if the inputs cannot form a valid triangle", () => {
        try {
            getTriangleArea(3, 4, 8);
        } catch (e) {
            expect((<Error>e).message).toEqual(expectedError);
        }
    });
});
