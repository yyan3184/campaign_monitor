import { findMostOccurrence } from "../question_4";

describe("question_4/findMostOccurrence", () => {
    it("should return 2 expected values as both are the most common in the array", () => {
        const expectedIntegers = [4, 5];
        const inputs = [5, 4, 3, 2, 4, 5, 1, 6, 1, 2, 5, 4];
        const result = findMostOccurrence(inputs);
        expect(result.sort()).toEqual(expectedIntegers.sort());
    });

    it("should return 1 expected value as the most common in the array", () => {
        const expectedIntegers = [1];
        const inputs = [1, 2, 3, 4, 5, 1, 6, 7];
        const result = findMostOccurrence(inputs);
        expect(result.sort()).toEqual(expectedIntegers.sort());
    });

    it("should return every value in the input array as each of them has 1 occurrence", () => {
        const inputs = [1, 2, 3, 4, 5, 6, 7];
        const expectedIntegers = inputs;
        const result = findMostOccurrence(inputs);
        expect(result.sort()).toEqual(expectedIntegers.sort());
    });
});
