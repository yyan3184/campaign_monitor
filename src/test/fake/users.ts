
export const users = [{
    id: 1,
    name: 'bob',
},
{
    id: 2,
    name: 'sally',
    age: 30,
},
{
    id: 3,
    name: 'bob',
    age: 30,
},
    null,
{
    id: 4,
    age: 31
},
{
    id: 5,
    name: 'cat',
    age: 31
},
{
    id: 5,
    name: 'nick',
    age: 31
}];


export interface User {
    id?: number;
    name?: string;
    age?: number
}

export interface ArrangedUsers { [key: string]: User[] }



export const expectedOutputByName: ArrangedUsers = {
    bob: [{ id: 1, name: 'bob' }, { id: 3, name: 'bob', age: 30 }],
    sally: [{ id: 2, name: 'sally', age: 30 }],
    cat: [{ id: 5, name: 'cat', age: 31 }],
    nick: [{ id: 5, name: 'nick', age: 31 }]
}


export const expectedOutputById: ArrangedUsers = {
    '1': [{ id: 1, name: 'bob' }],
    '2': [{ id: 2, name: 'sally', age: 30 }],
    '3': [{ id: 3, name: 'bob', age: 30 }],
    '4': [{ id: 4, age: 31 }],
    '5': [{ id: 5, name: 'cat', age: 31 }, { id: 5, name: 'nick', age: 31 }]
}