import { isNullOrEmpty } from "../question_1";
describe("question_1/isNullOrEmpty", () => {
    it("should return true if it is a null", () => {
        expect(isNullOrEmpty(null)).toBe(true);
    });
    it("should return false if it is a valid string with value `a`", () => {
        expect(isNullOrEmpty("a")).toBe(false);
    });
    it("should return true if it is an empty string", () => {
        expect(isNullOrEmpty("")).toBe(true);
    });
    it("should return false if it is a valid string with value `null`", () => {
        expect(isNullOrEmpty("null")).toBe(false);
    });
});
