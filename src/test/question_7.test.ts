import { arrangeBy } from "../question_7";
import { users, expectedOutputByName, ArrangedUsers, expectedOutputById } from './fake/users'

describe("question_7/arrangeBy", () => {
    it("if the key you provided is not present in an object, should exclude that from the output", () => {
        const expectedRemovedId = 4;
        const arrangeByName = arrangeBy('name');
        const result: ArrangedUsers = arrangeByName(users);

        //test the arranged output by key name equals to the expected output
        expect(result).toStrictEqual(expectedOutputByName);

        //test that the element without the key as property, is now excluded from the output
        Object.values(result).flat().forEach(user => { expect(user.id).not.toEqual(expectedRemovedId) });
    });

    it("if an element of the array is null/undefined, or not an object ­- exclude that from the output", () => {
        const nullCount = 1;
        const duplicateKey = 5;
        const totalCountBefore = users.length;

        const arrangeById = arrangeBy('id');
        const result: ArrangedUsers = arrangeById(users);

        //test the arranged output by key id equals to the expected output
        expect(result).toStrictEqual(expectedOutputById);

        //test if the null element is excluded from the output
        const totalCountAfter = Object.values(result).flat().length;
        expect(totalCountAfter).toEqual(totalCountBefore - nullCount);

        //test that if multiple objects are with the same key, then attach them to the same key as an array.
        expect(result[duplicateKey].length).toEqual(2);
    });

});
