import { findPositiveDivisors } from "../question_2";

describe("question_2/findPositiveDivisors", () => {
    it("should only return 1 and the given value itself as the positive divisors", () => {
        const expectedDivisors = [1, 3];
        const result = findPositiveDivisors(3);
        expect(result.sort()).toEqual(expectedDivisors.sort());
    });

    it("should return the expected values ranged from 1 to 60 as the positive divisors", () => {
        const expectedDivisors = [1, 2, 3, 4, 5, 6, 10, 12, 15, 20, 30, 60];
        const result = findPositiveDivisors(60);
        expect(result.sort()).toEqual(expectedDivisors.sort());
    });

    it("should return the expected values ranged from 1 to 42 as the positive divisors", () => {
        const expectedDivisors = [1, 2, 3, 6, 7, 14, 21, 42];
        const result = findPositiveDivisors(42);
        expect(result.sort()).toEqual(expectedDivisors.sort());
    });
});
