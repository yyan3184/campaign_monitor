import { JSDOM } from 'jsdom';

export const linkChecker = async (htmlInput: string) => {

    (<any>global).XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

    const dom = new JSDOM(htmlInput);
    const doc = dom.window.document;

    const linkObjects = Array.from(doc.links);
    const links = linkObjects.map(link => link.href);

    const checkedLinks = await Promise.all(
        links.map(link => validateLink(link)));
    const validLinks = checkedLinks.filter(link => !!link);
    console.log('valid links: ', validLinks);
    return validLinks;
}

export const validateLink = (link: string) => {
    return new Promise((resolve, _) => {
        const req = new XMLHttpRequest();
        req.open('GET', link);
        req.onload = () => {
            if (req.status === 200) {
                resolve(link);
            }
            else {
                resolve('');
            }
        };
        req.onerror = () => {
            resolve('');
        };
        req.send();
    });
}


