export const getTriangleArea = (sideA: number, sideB: number, sideC: number) => {
    if (areAllLengthsValid(sideA, sideB, sideC)) {
        const s = (sideA + sideB + sideC) / 2;
        const area = Math.sqrt(s * ((s - sideA) * (s - sideB) * (s - sideC)));
        return area;
    } else {
        throw Error('InvalidTriangleException');
    }
}

const areAllLengthsValid = (sideA: number, sideB: number, sideC: number) => {
    return (sideA + sideB > sideC && sideA + sideC > sideB && sideB + sideC > sideA)
        && (sideA > 0 && sideB > 0 && sideC > 0);
}
