export const isNullOrEmpty = (str:string|null) => {
    return (!str || str.length === 0);
}
