create table Salesperson (SalespersonID integer , Name varchar(255), Age integer, Salary integer);
insert into Salesperson values ( 1, 'Alice', 61, 140000);
insert into Salesperson values ( 2, 'Bob', 34, 44000);
insert into Salesperson values ( 6, 'Chris', 34, 40000);
insert into Salesperson values ( 8, 'Derek', 41, 52000);
insert into Salesperson values ( 11, 'Emmit', 57, 115000);
insert into Salesperson values ( 16, 'Fred', 38, 38000);

create table Customer (CustomerID integer , Name varchar(255));
insert into Customer values ( 4, 'George');
insert into Customer values ( 6, 'Harry');
insert into Customer values ( 7, 'Ingrid');
insert into Customer values ( 11, 'Jerry');

create table Orders (OrderID integer, OrderDate varchar(255), CustomerID integer, SalespersonID integer, NumberOfUnits integer, CostOfUnit integer);
insert into Orders values (3, '17/01/2013', 4, 2, 4, 400);
insert into Orders values (6, '07/02/2013', 4, 1, 1, 600);
insert into Orders values (10, '04/03/2013', 7, 6, 2, 300);
insert into Orders values (17, '15/03/2013', 6, 1, 5, 300);;
insert into Orders values (25, '19/04/2013', 11, 11, 7, 300);
insert into Orders values (34, '22/04/2013', 11, 11, 100, 26);
insert into Orders values (57, '12/07/2013', 7, 11, 14, 11);


-- Question 5 - A

WITH Georges_Order AS (
    SELECT OrderID, SalespersonID
    FROM Orders ord
    LEFT JOIN Customer cust
        ON ord.CustomerID = cust.CustomerID
    WHERE cust.Name = 'George'
)

SELECT DISTINCT Name
FROM  Salesperson
WHERE SalespersonID IN (
    SELECT DISTINCT 
        SalespersonID 
    FROM Georges_Order);
    
-- Question 5 - B  

WITH Georges_Order AS (
    SELECT OrderID, SalespersonID
    FROM Orders ord
    LEFT JOIN Customer cust
        ON ord.CustomerID = cust.CustomerID
    WHERE cust.Name = 'George'
)

SELECT DISTINCT Name
FROM  Salesperson
WHERE SalespersonID NOT IN (
    SELECT DISTINCT 
        SalespersonID 
    FROM Georges_Order);

-- Question 5 - C

SELECT DISTINCT sale.Name
FROM Salesperson sale
LEFT JOIN Orders ord
    ON ord.SalespersonID = sale.SalespersonID
GROUP BY sale.Name
HAVING COUNT(ord.OrderID) >= 2


-- Question 6 - A
WITH RANKED_SALES_SALARY AS (
SELECT Name, RANK() OVER (ORDER BY Salary DESC) AS SalaryRank
FROM Salesperson
)
SELECT NAME
FROM RANKED_SALES_SALARY
WHERE SalaryRank = 3;

-- Question 6 - B
CREATE TABLE BigOrders (CustomerID integer , TotalOrderValue integer);
INSERT INTO BigOrders  
SELECT CustomerID, SUM(NumberOfUnits * CostOfUnit) AS TotalOrderValue
FROM Orders
GROUP BY CustomerID
HAVING SUM(NumberOfUnits * CostOfUnit) > 1000;

SELECT * FROM BigOrders;

-- Question 6 - C
SELECT 
     SUM(NumberOfUnits * CostOfUnit) as TotalAmount
    ,EXTRACT(YEAR FROM TO_DATE(OrderDate, 'DD/MM/YYYY')) AS OrderYear
    ,EXTRACT(MONTH FROM TO_DATE(OrderDate, 'DD/MM/YYYY')) AS OrderMonth
FROM Orders
GROUP BY 2,3
ORDER BY 2 DESC, 3 DESC


    
