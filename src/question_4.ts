export const findMostOccurrence = (inputs: number[]) => {
    let most = 0;
    const mapping = inputs.reduce((accum, input: number) => {
        if (accum[input]) {
            accum[input]++;
        } else {
            accum[input] = 1;
        }
        if (accum[input] > most) {
            most = accum[input];
        }
        return accum;
    }, {} as { [key: number]: number });
    return Array.from(new Set(inputs.filter(input => mapping[input] === most)));
}