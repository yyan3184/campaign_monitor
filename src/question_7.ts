export const arrangeBy = (key: string) => (inputs: any[]) => {
    const arranged = inputs.reduce((accum, element: any) => {
        if (element && element[key]) {
            const indexValue = element[key];
            if (accum[indexValue]) {
                accum[indexValue].push(element);
            } else {
                accum[indexValue] = [element];
            }
        }
        return accum;
    }, {} as any);
    return arranged;
}

export const users = [{
    id: 1,
    name: 'bob',
},
{
    id: 2,
    name: 'sally',
    age: 30,
},
{
    id: 3,
    name: 'bob',
    age: 30,
},
    null,
{
    id: 4,
    age: 31
},
{
    id: 5,
    name: 'cat',
    age: 31
},
{
    id: 5,
    name: 'nick',
    age: 31
}];
