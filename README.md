# README #

This README is used to document the steps are necessary to set up the campaign monitor's code testing project. 

* It consists of a node project written in **TypeScript** for question 1,2,3,4,7 and 8. 
* The question 5 and 6 is written in **PostgreSQL 11.10**.


### Local set up and running for question 1,2,3,4,7 and 8 ###

- Clone this git repo to your local machine `git clone https://yyan3184@bitbucket.org/yyan3184/campaign_monitor.git`

- Go to the root directory of the project

- Run `npm i` to install all the required dependencies

- Run `npm run test` for unit test and check the verbose info of each test case for each question

- Run `npm run dev` so that it will print logs for each function with different input parameters, and check how each function return the expected result


### SQL questions: 5, 6 ###

- For SQL questions, find the file `/src/SQL.sql` (I renamed it using `.sql` instead of `.txt` to utilise the syntax highlight in VS Code)

- The first section of queries I implemented the tables creation and rows insert

- For the following sql queries are all written in PostgreSQL and should be no database-specific features

- There are some CTE(common table expression) used for temporary named result set - mainly for the customer George's orders
